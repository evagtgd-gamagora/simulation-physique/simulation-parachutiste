﻿using System;
using System.Collections.Generic;
using System.Numerics;


namespace Simulation
{
    
    class SimDrop
    {
        public const float g = 9.80665f;
        public const float dragCoefParachute = 1.75f;
        public const float heightOpeningParachute = 1000f;

        float mass;
        float dragCoef;
        Vector3 p0;
        Vector3 v0;
        Vector3 a0;

        public SimDrop(float mass, float dragCoef, Vector3 p0, Vector3 v0, Vector3 a0)
        {
            if (mass < 0.1)
                throw new System.ArgumentException("Negative mass");
            if (dragCoef < 0)
                throw new System.ArgumentException("Negative drag coefficient");
            if (p0.Z < 0)
                throw new System.ArgumentException("Initial postion underground");

            this.mass = mass;
            this.dragCoef = dragCoef;
            this.p0 = p0;
            this.v0 = v0;
            this.a0 = a0;
        }

        public SimRec Simulation(float t0, float deltaTime, float windSpeed)
        {
            if (deltaTime < 0)
                throw new System.ArgumentException("Negative deltaTime");

            SimRec simulation = new SimRec();
            float t = 0;
            Vector3 pt = p0;
            Vector3 vt = v0;
            Vector3 at = a0;

            simulation.AddSample(t, pt, vt, at);

            Vector3 weight = g * Vector3.UnitZ; // * mass

            while(t < t0)
            {
                t += deltaTime;
                pt += vt * deltaTime;
                pt += -deltaTime * windSpeed * Vector3.UnitX;
                simulation.AddSample(t, pt, vt, at);
            }

            while (pt.Z > heightOpeningParachute)
            {
                float td = t + deltaTime;
                Vector3 atd = - weight - (dragCoef / mass) * vt.Length() * vt;

                Vector3 vtd = deltaTime * at + vt;
                Vector3 ptd = deltaTime * vt + pt;
                //Wind move
                ptd += -deltaTime * windSpeed * Vector3.UnitX;
                
                simulation.AddSample(td, ptd, vtd, atd);

                t = td;
                pt = ptd;
                vt = vtd;
                at = atd;
            }

            dragCoef = dragCoefParachute;
            while (pt.Z > 0)
            {
                float td = t + deltaTime;
                Vector3 atd = -weight - (dragCoef / mass) * vt.Length() * vt;

                Vector3 vtd = deltaTime * at + vt;
                Vector3 ptd = deltaTime * vt + pt;
                //Wind move
                //ptd += -deltaTime * windSpeed * Vector3.UnitX;

                simulation.AddSample(td, ptd, vtd, atd);

                t = td;
                pt = ptd;
                vt = vtd;
                at = atd;
            }

            return simulation;
        }
    }
}
