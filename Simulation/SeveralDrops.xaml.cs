﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Numerics;
using LiveCharts;
using LiveCharts.Wpf;

namespace Simulation
{
    /// <summary>
    /// Logique d'interaction pour SeveralDrops.xaml
    /// </summary>
    public partial class SeveralDrops : Window
    {
        int displayStep = 10;
        public SeriesCollection SeriesCollection { get; set; }
        public Func<float, string> YFormatter { get; set; }

        public string[] Labels { get; set; }

        public SeveralDrops()
        {
            float deltaTime = 0.1f;
            
            Vector3 p0 = new Vector3(0, 0, 4000);
            Vector3 v0 = new Vector3(45, 0, 0);
            Vector3 a0 = new Vector3(0, 0, 0);

            float windSpeed = 50;


            float massA = 90f;
            float vMaxA = 50f;
            float dragCoefA = massA * SimDrop.g / (vMaxA * vMaxA);

            SimDrop simA = new SimDrop(massA, dragCoefA, p0, v0, a0);
            SimRec simResultA = simA.Simulation(0, deltaTime, windSpeed);

            float massB = 90f;
            float vMaxB = 80f;
            float dragCoefB = massB * SimDrop.g / (vMaxB * vMaxB);

            SimDrop simB = new SimDrop(massB, dragCoefB, p0, v0, a0);
            SimRec simResultB = simB.Simulation(10, deltaTime, windSpeed);

            InitializeComponent();


            //Values data
            ChartValues<float> posXa = new ChartValues<float>();
            ChartValues<float> posZa = new ChartValues<float>();
            ChartValues<float> posXb = new ChartValues<float>();
            ChartValues<float> posZb = new ChartValues<float>();


            for (int i = 0; i < simResultA.positions.Count; i += displayStep)
            {
                posXa.Add(simResultA.positions[i].X);
                posZa.Add(simResultA.positions[i].Z);
            }

            for (int i = 0; i < simResultB.positions.Count; i += displayStep)
            {
                posXb.Add(simResultB.positions[i].X);
                posZb.Add(simResultB.positions[i].Z);
            }

            //Define Series
            SeriesCollection = new SeriesCollection { };

            SeriesCollection.Add(new LineSeries
            {
                Title = "PosX-A",
                Values = posXa,
                ScalesYAt = 0
            });
            SeriesCollection.Add(new LineSeries
            {
                Title = "PosZ-A",
                Values = posZa,
                ScalesYAt = 1
            });

            SeriesCollection.Add(new LineSeries
            {
                Title = "PosX-B",
                Values = posXb,
                ScalesYAt = 0
            });

            SeriesCollection.Add(new LineSeries
            {
                Title = "PosZ-B",
                Values = posZb,
                ScalesYAt = 1
            });



            List<string> labelTimeValues = new List<string>();

            if(simResultA.timeStamps.Count > simResultB.timeStamps.Count)
                foreach (float time in simResultA.timeStamps)
                {
                    labelTimeValues.Add(time.ToString());
                }
            else
                foreach (float time in simResultB.timeStamps)
                {
                    labelTimeValues.Add(time.ToString());
                }

            Labels = labelTimeValues.ToArray();
            YFormatter = value => value.ToString("C");

            DataContext = this;
        }
    }


}
