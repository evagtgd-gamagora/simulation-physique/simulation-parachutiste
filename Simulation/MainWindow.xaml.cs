﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;
using System.Numerics;

namespace Simulation
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int displayStep = 10;
        public SeriesCollection SeriesCollection { get; set; }
        public Func<float, string> YFormatter { get; set; }

        public string[] Labels { get; set; }

        public MainWindow()
        {
            float mass = 90;
            float dragCoef = 0.35f;
            Vector3 p0 = new Vector3(0, 0, 4000);
            Vector3 v0 = new Vector3(45, 0, 0);
            Vector3 a0 = new Vector3(0, 0, 0);

            float deltaTime = 0.1f;

            SimDrop sim1 = new SimDrop(mass, dragCoef, p0, v0, a0);
            SimRec simResult1 = sim1.Simulation(0, deltaTime, 50);

            InitializeComponent();

            //Values data
            ChartValues<float> posX = new ChartValues<float>();
            ChartValues<float> posZ = new ChartValues<float>();

            ChartValues<float> speedX = new ChartValues<float>();
            ChartValues<float> speedZ = new ChartValues<float>();

            ChartValues<float> accelX = new ChartValues<float>();
            ChartValues<float> accelZ = new ChartValues<float>();

            
            for (int i = 0; i < simResult1.positions.Count; i+= displayStep)
            {
                posX.Add(simResult1.positions[i].X);
                posZ.Add(simResult1.positions[i].Z);
            }

            for (int i = 0; i < simResult1.speeds.Count; i += displayStep)
            {
                speedX.Add(simResult1.speeds[i].X);
                speedZ.Add(simResult1.speeds[i].Z);
            }

            for (int i = 0; i < simResult1.accelerations.Count; i += displayStep)
            {
                accelX.Add(simResult1.accelerations[i].X);
                accelZ.Add(simResult1.accelerations[i].Z);
            }

            //Define Series
            SeriesCollection = new SeriesCollection{ };

            SeriesCollection.Add(new LineSeries
            {
                Title = "PosX",
                Values = posX,
                ScalesYAt = 0
            });
            SeriesCollection.Add(new LineSeries
            {
                Title = "PosZ",
                Values = posZ,
                ScalesYAt = 0
            });

            SeriesCollection.Add(new LineSeries
            {
                Title = "SpeedX",
                Values = speedX,
                ScalesYAt = 1
            });

            SeriesCollection.Add(new LineSeries
            {
                Title = "SpeedZ",
                Values = speedZ,
                ScalesYAt = 1
            });

            SeriesCollection.Add(new LineSeries
            {
                Title = "AccelX",
                Values = accelX,
                ScalesYAt = 2
            });

            SeriesCollection.Add(new LineSeries
            {
                Title = "AccelZ",
                Values = accelZ,
                ScalesYAt = 2
            });


            List<string> labelTimeValues = new List<string>();
            foreach (float time in simResult1.timeStamps)
            {
                labelTimeValues.Add(time.ToString());
            }

            Labels = labelTimeValues.ToArray();
            YFormatter = value => value.ToString("C");

            DataContext = this;

            SeveralDrops subWindow = new SeveralDrops();
            subWindow.Show();
        }

    }
}
