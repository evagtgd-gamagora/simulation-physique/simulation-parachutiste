﻿using System.Collections.Generic;
using System.Numerics;
using LiveCharts;
using LiveCharts.Wpf;

namespace Simulation
{
    class SimRec
    {
        public List<float> timeStamps = new List<float>();
        public List<Vector3> positions = new List<Vector3>();
        public List<Vector3> speeds = new List<Vector3>();
        public List<Vector3> accelerations = new List<Vector3>();


        public SimRec() { }

        public void AddSample(float time, Vector3 position, Vector3 speed, Vector3 acceleration)
        {
            timeStamps.Add(time);
            positions.Add(position);
            speeds.Add(speed);
            accelerations.Add(acceleration);
        }
    }
}
